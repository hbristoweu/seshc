/* SELinux SHow Context (seshc)
 *
 * Parameters:
 * 	1. Path to file or directory
 *
 * Returns:
 * 	Program success errorlevel (Posix)
 *
 * Example:
 * 	$ seshc /etc/selinux/config
 * 	> system_u:object_r:selinux_config_t:s0
 *
 * Made as an exercise in building / compiling selinux programs in C
 *
 * * Requries libselinux-devel
 * * Build with -lselinux
 */

#include <selinux/selinux.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>

int main(int argc, char* argv[]){

	char *faddr, *label;
	security_context_t ctx;

	if (argc != 2){

		return(-1);

	} else {

		faddr = argv[1];

		if (getfilecon(faddr, &ctx) < 0)

			perror("Failed to get selinux context");

		else {

			label = strdup((char *)ctx);

			freecon(ctx);

			fprintf(stdout, "%s\n", label);

		}

	}

	return(0);
}
