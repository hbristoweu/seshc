## SELinux SHow Context (seshc)
SELinux program that prints the context of a given object (File/Directory)

Made as an exercise in building / compiling selinux programs in C

* Requries libselinux-devel
* Build with -lselinux

# Parameters:
	1. Path to file or directory

# Returns:
	Program success errorlevel (Posix)

# Example:
	$ seshc /etc/selinux/config
	> system_u:object_r:selinux_config_t:s0




